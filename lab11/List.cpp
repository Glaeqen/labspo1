#include "List.h"

void GreatNode::setNext(GreatNode *next){
	next_ = next;
}

GreatNode *GreatNode::next() const{
	return next_;
}

GreatNode::~GreatNode(){
	if(!next_) return;
	delete next_;
	next_ = 0;
}

List::List(): head_(0){}

GreatNode *List::head(){
	return head_;
}

List::~List(){
	if(!head_) return; 
	delete head_;
	head_ = 0;
}

