template <typename T>
bool GreatNode::isType() const{
	return vIsType(typeid(T));
}

template <typename T>
Node<T>::Node(T value): value_(value){}

template <typename T>
bool Node<T>::vIsType(const std::type_info& typeinfo) const{
	return typeinfo == typeid(T);
}

template <typename T>
T Node<T>::getValue() const{
	return value_;
}

template <typename T>
T List::get(GreatNode *node){
	return dynamic_cast<Node<T>*>(node)->getValue();
}

template <typename T>
void List::add(T input){
	GreatNode *tmpNode = head_;
	head_ = new Node<T>(input);
	head_->setNext(tmpNode);
}