#pragma once
#include <typeinfo>

class GreatNode{
	GreatNode *next_;
public:
	virtual bool vIsType(const std::type_info& typeinfo) const = 0;
	
	template <typename T>
	bool isType() const;

	void setNext(GreatNode *next);
	GreatNode *next() const;

	virtual ~GreatNode();
};

template <typename T>
class Node : public GreatNode{
	T value_;
public:
	Node(T value);
	
	virtual bool vIsType(const std::type_info& typeinfo) const;

	T getValue() const;
};

// List class which supports variety of types 
// that can be stored in it (at the same time)
class List{
	GreatNode *head_;
public:
	// Default constructor - prepares object for further usage
	List();

	// Template method which returns value from specified pointer
	// to list's node.
	// Correct type parameter is required. Method is not safe for
	// wrong types given.
	template <typename T>
	static T get(GreatNode *node);

	// Adds 'input' element at the beginning of the list
	template <typename T>
	void add(T input);

	// Returns head of the list
	GreatNode *head();

	// Destructor. Frees allocated memory.
	~List();
};

#include "List.tpp"
