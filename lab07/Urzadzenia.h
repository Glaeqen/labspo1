#pragma once
#include <string>
class Rozdzielczosc{
	std::string rozdzielczosc_;
public:
	Rozdzielczosc(unsigned width, unsigned height);
	std::string getRozdzielczosc() const;
};

class Urzadzenie{
public:
	virtual std::string rodzaj() const = 0;
	virtual ~Urzadzenie();
};

class Skaner : public virtual Urzadzenie{
	Rozdzielczosc roz_;
public:
	Skaner(const Rozdzielczosc& roz);

	std::string rozdzielczosc() const;
	std::string rodzaj() const;
};

class Papierozerne : public virtual Urzadzenie{
public:
	std::string rodzaj() const = 0;
};

class Ksero : public virtual Papierozerne{
	Rozdzielczosc roz_;
public:
	Ksero(const Rozdzielczosc& roz);

	std::string rozdzielczosc() const;
	std::string rodzaj() const;
};

class Drukarka : public virtual Papierozerne{
	Rozdzielczosc roz_;
public:
	Drukarka(const Rozdzielczosc& roz);
	std::string rozdzielczosc() const;
	std::string rodzaj() const;
};

class Wielofunkcyjne : public virtual Skaner, public virtual Ksero, public virtual Drukarka{
public:
	Wielofunkcyjne(const Rozdzielczosc& roz1, const Rozdzielczosc& roz2);
	std::string rodzaj() const;
};

// The most important thing is that class 'Wielofunkcyjne' is extending virtually classes
// 'Skaner', 'Ksero', 'Drukarka' because of the necessity of initializing in a different
// way those classes from 'Wielofunkcyjna' in that specific situation and casting. Otherwise it wouldn't be possible. 
// That way of approaching problems should be avoided.
// Extending other classes is trivial