#include "Urzadzenia.h"
#include <cstdio>

Rozdzielczosc::Rozdzielczosc(unsigned width, unsigned height){
	char temp[50] = {0};
	sprintf(temp, "%dx%d", width, height);
	rozdzielczosc_ = std::string(temp);
}

std::string Rozdzielczosc::getRozdzielczosc() const{
	return rozdzielczosc_;
}

Urzadzenie::~Urzadzenie(){}

Skaner::Skaner(const Rozdzielczosc& roz) : roz_(roz){}

std::string Skaner::rozdzielczosc() const{
	return roz_.getRozdzielczosc();
}
std::string Skaner::rodzaj() const{
	return std::string("Skaner");
}

Ksero::Ksero(const Rozdzielczosc& roz) : roz_(roz){}

std::string Ksero::rozdzielczosc() const{
	return roz_.getRozdzielczosc();
}

std::string Ksero::rodzaj() const{
	return std::string("Ksero");
}

Drukarka::Drukarka(const Rozdzielczosc& roz) : roz_(roz){}

std::string Drukarka::rozdzielczosc() const{
	return roz_.getRozdzielczosc();
}

std::string Drukarka::rodzaj() const{
	return std::string("Drukarka");
}

Wielofunkcyjne::Wielofunkcyjne(const Rozdzielczosc& roz1, const Rozdzielczosc& roz2) : Skaner(roz2), Ksero(roz1), Drukarka(roz1){}

std::string Wielofunkcyjne::rodzaj() const{
	return std::string("Wielofunkcyjne");
}


