#pragma once
#include <iostream>

using std::ostream;
using std::cout;
using std::endl;

class IntSequence{
	// Private fields
	// Hold an array of integers, with volume of arraySize_
	int *intArray_;
	int arraySize_;
	// An iterator
	int currentPos_;

	// Private methods
	// Increases an array size by 1
	void increaseArraySize();
	// Adds new element to an array
	void addElement(int newElement);
public:
	// Public constructor
	// Initializes fields with zeros
	IntSequence();
	// Adds new element right to the int array
	IntSequence& operator<<(int right);

	// Const public methods
	// Returns an integer currently indicated by iterator from the int array
	int operator()() const;
	// Converts this object to the integer. Its value is an integer currently indicated by iterator from the int array
	operator int() const;
	// Returns true if iterator gets to an end of int array
	bool finished() const;

	// Non-const public methods
	// Resets iterator to zero.
	void reset();
	// Increments iterator by 1
	const IntSequence& operator++(int);
	// Decrements iterator by 1
	const IntSequence& operator--(int);

	// Public destructor
	// Clears this object
	~IntSequence();

	// Friend allows to get private fields from IntSequence object for << operator for ostream
	friend ostream& operator<<(ostream& cout, const IntSequence& right);
};

// Global ostream << operator overload for IntSequence class
ostream& operator<<(ostream& cout, const IntSequence& right);