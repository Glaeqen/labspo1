#include "IntSequence.h"

void IntSequence::increaseArraySize(){
	int *newIntArray = new int[arraySize_+1];

	for(int i=0; i<arraySize_; i++){
		newIntArray[i] = intArray_[i];
	}

	if(intArray_)	delete[] intArray_;
	intArray_ = newIntArray;
	arraySize_++;
}

void IntSequence::addElement(int newElement){
	increaseArraySize();
	intArray_[arraySize_-1] = newElement;
}

IntSequence::IntSequence(): intArray_(NULL), arraySize_(0), currentPos_(0){}

IntSequence& IntSequence::operator<<(int right){
	addElement(right);
	return *this;
}

int IntSequence::operator()() const{
	return intArray_[currentPos_ % arraySize_];
}

IntSequence::operator int() const{
	return (*this)();
}

bool IntSequence::finished() const{
	return currentPos_ >= arraySize_;
}

void IntSequence::reset(){
	currentPos_ = 0;
}

const IntSequence& IntSequence::operator++(int){
	currentPos_++;
	return *this;
}

const IntSequence& IntSequence::operator--(int){
	currentPos_--;
	return *this;
}

IntSequence::~IntSequence(){
	delete[] intArray_;
	arraySize_ = 0;
	currentPos_ = 0;
}

ostream& operator<<(ostream& cout, const IntSequence& right){
	return cout << right.intArray_[right.currentPos_];
}

