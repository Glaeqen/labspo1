#pragma once

// Class representing a point with integer type coordinates
class Punkt{
	int x_;
	int y_;
public:
	// Constructor initialises Point object with x and y parameters.
	Punkt(int x, int y);
	// Returns coordinate choosen by template int parameter (0: x, 1: y)
	template <int coord>
	int wsp() const;

	// Returns minimum of a and b objects 
	// (T::operator<(T) / operator<(T, T) defined required)
	template <typename T>
	static T min(T a, T b);

	// Returns maximum of a and b objects ()
	// (T::operator<(T) / operator<(T, T) defined required)
	template <typename T>
	static T max(T a, T b);

	// Return true if this object has got smaller x coordinate than 'punkt'
	// (if x coordinate is equal than returns true if this object's 'y'
	// coordinate is smaller than 'punkt's one)
	bool operator <(const Punkt& punkt) const;
};

template <int coord>
int Punkt::wsp() const{
	if(!coord) return x_;
	return y_;
}

template <typename T>
T Punkt::min(T a, T b){
	if(a < b) return a;
	return b;
}

template <typename T>
T Punkt::max(T a, T b){
	if(a < b) return b;
	return a;
}