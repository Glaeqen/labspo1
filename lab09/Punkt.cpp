#include "Punkt.h"

Punkt::Punkt(int x, int y): x_(x), y_(y){}

bool Punkt::operator <(const Punkt& punkt) const{
	if(x_ != punkt.x_) return x_ < punkt.x_;
	return y_ < punkt.y_;
}