#pragma once

#include <string>

class TestClassA{
	std::string name_;
public:
	//Public constructor
	//Creates TestClassA object initialized with `name`.
	TestClassA(const char *name);

	//Const methods:
	//Returns name of an object.
	const char *name() const;

	//Public destructor
	//Destroys object.
	~TestClassA();
};