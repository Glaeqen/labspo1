#pragma once

#include "TestClassA.h"

class SmartPointer{
	TestClassA *protectedPtr_;
	unsigned *aliveReferences_;
public:
	//Public constructors:
	//Creates SmartPointer object initialized with specified pointer to protect.
	SmartPointer(TestClassA *ptrToProtect);
	//Creates SmartPointer object which is copy of `smartPointer`.
	SmartPointer(const SmartPointer& smartPointer);

	//Operators overloading:
	//Overwrites >left operand object< with >right operand object<.
	void operator=(const SmartPointer& right);
	//-> and * overloading makes SmartPointer object to behave like pointer
	//in terms of dereferencing.
	const TestClassA *operator->() const;
	const TestClassA& operator*() const;

	//Public destructor:
	//Smart destructor which deletes an object when there is no more alive references.
	~SmartPointer();
};