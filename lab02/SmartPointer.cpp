#include "SmartPointer.h"

SmartPointer::SmartPointer(TestClassA *ptrToProtect){
	aliveReferences_ = new unsigned;

	protectedPtr_ = ptrToProtect;
	*aliveReferences_ = 1;
}

SmartPointer::SmartPointer(const SmartPointer& smartPointer){
	protectedPtr_ = smartPointer.protectedPtr_;
	aliveReferences_ = smartPointer.aliveReferences_;
	(*aliveReferences_)++;
}

void SmartPointer::operator=(const SmartPointer& right){
	if(this == &right)	return;

	this->~SmartPointer();

	protectedPtr_ = right.protectedPtr_;
	aliveReferences_ = right.aliveReferences_;
	(*aliveReferences_)++;
}

const TestClassA *SmartPointer::operator->() const{
	return protectedPtr_;
}

const TestClassA& SmartPointer::operator*() const{
	return *protectedPtr_;
}

SmartPointer::~SmartPointer(){
	(*aliveReferences_)--;

	if(!*aliveReferences_){
		delete protectedPtr_;
		delete aliveReferences_;
	}

	protectedPtr_ = 0;
	aliveReferences_ = 0;
}