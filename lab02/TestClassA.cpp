#include <cstring>
#include <iostream>

using std::cout;
using std::endl;

#include "TestClassA.h"

TestClassA::TestClassA(const char *name): name_(name){
	cout << ".. Konstruuje TestClassA " << name_ << endl;
}

const char *TestClassA::name() const{
	return name_.c_str();
}

TestClassA::~TestClassA(){
	cout << ".. Usuwam  TestClassA " << name_ << endl;
}