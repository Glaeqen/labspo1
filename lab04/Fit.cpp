#include "Fit.h"
#include <iostream>

Fit::Fit(const char *name):name_(name){}
 
void Fit::appendPoint(int x, double y) const{
	std::cout << "Cannot add point (" << x << ", " << y << "), to const object." << std::endl;
}

void Fit::print() const{
	std::cout << "This is " << name_ << std::endl;
}