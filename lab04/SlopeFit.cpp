#include "SlopeFit.h"
#include <vector>
#include <string>

SlopeFit::SlopeFit(const char *name):
	Fit(name),
	sumxiyi_(0.0),
	sumxixi_(0.0)
{}

double SlopeFit::getSumxiyi() const{
	return sumxiyi_;
}
double SlopeFit::getSumxixi() const{
	return sumxixi_;
}

void SlopeFit::appendPoint(int x, double y){
	sumxiyi_ += x*y;
	sumxixi_ += x*x;
}

FitResult SlopeFit::result() const{
	double a = sumxiyi_/sumxixi_;

	std::vector<std::string> parameterNames;
	std::vector<double> parameterValues;

	parameterNames.push_back("a");
	parameterValues.push_back(a);

	return FitResult("y = a * x", parameterNames, parameterValues);
}