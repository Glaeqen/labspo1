#pragma once
#include <string>
#include "FitResult.h"

class Fit{
	std::string name_;
public:
	Fit(const char *name);

	void appendPoint(int x, double y) const;
	virtual void appendPoint(int x, double y) = 0;
	virtual FitResult result() const = 0;

	void print() const;
};