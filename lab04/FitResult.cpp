#include "FitResult.h"

FitResult::FitResult(const char *expression, const std::vector<std::string>& parameterNames, const std::vector<double>& parameterValues):
	expression_(expression),
	parameterNames_(parameterNames),
	parameterValues_(parameterValues)
{}

std::string FitResult::expression() const{
	return expression_;
}

std::string FitResult::parameterName(unsigned index) const{
	return parameterNames_[index];
}

double FitResult::parameterValue(unsigned index) const{
	return parameterValues_[index];
}

unsigned FitResult::nParams() const{
	return parameterNames_.size();
}