#include "LineFit.h"
#include <vector>
#include <string>

LineFit::LineFit(const char *name):
	SlopeFit(name),
	sumxi_(0.0),
	sumyi_(0.0),
	n_(0)
{}

void LineFit::appendPoint(int x, double y){
	SlopeFit::appendPoint(x, y);
	sumxi_ += x;
	sumyi_ += y;
	n_++;
}

FitResult LineFit::result() const{
	double a = (getSumxiyi() - sumxi_ * sumyi_/n_)/(getSumxixi() - sumxi_ * sumxi_/n_);
	double b = (sumyi_ - a * sumxi_)/n_;

	std::vector<std::string> parameterNames;
	std::vector<double> parameterValues;

	parameterNames.push_back("a");
	parameterValues.push_back(a);
	parameterNames.push_back("b");
	parameterValues.push_back(b);
	
	return FitResult("y = a * x + b", parameterNames, parameterValues);
}