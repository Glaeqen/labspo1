#pragma once
#include "Fit.h"
#include "FitResult.h"


class SlopeFit : public Fit{
	double sumxiyi_;
	double sumxixi_;
public:
	SlopeFit(const char *name);

	double getSumxiyi() const;
	double getSumxixi() const;

	virtual void appendPoint(int x, double y);
	virtual FitResult result() const;
};