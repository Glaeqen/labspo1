#pragma once
#include <string>
#include <vector>

class FitResult{
	std::string expression_;
	std::vector<std::string> parameterNames_;
	std::vector<double> parameterValues_;
public:
	FitResult(const char *expression, const std::vector<std::string>& parameterNames, const std::vector<double>& parameterValues);

	std::string expression() const;
	std::string parameterName(unsigned index) const;
	double parameterValue(unsigned index) const;
	unsigned nParams() const;
};