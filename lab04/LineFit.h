#pragma once
#include "SlopeFit.h"
#include "FitResult.h"

class LineFit : public SlopeFit{
	double sumxi_;
	double sumyi_;
	unsigned n_;
public:
	LineFit(const char *name);

	virtual void appendPoint(int x, double y);
	virtual FitResult result() const;
};