#pragma once

#include "SingleOperation.h"
#include "DoubleOperation.h"

struct OR : public DoubleOperation{
	virtual bool perform(bool first, bool second) const;
};

struct AND : public DoubleOperation{
	virtual bool perform(bool first, bool second) const;
};

struct XOR : public DoubleOperation{
	virtual bool perform(bool first, bool second) const;
};

struct NOT : public SingleOperation{
	virtual bool perform(bool value) const;
};

struct NOOP : public SingleOperation{
	virtual bool perform(bool value) const;
};