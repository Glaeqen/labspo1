#pragma once

// SingleOperation abstract class to extend from for single argument logical operations
// eg. negation

class SingleOperation{
public:
	virtual bool perform(bool value) const = 0;
};