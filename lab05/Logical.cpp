#include "Logical.h"

bool Logical::eval(const SingleOperation& operation, bool first){
	return operation.perform(first);
}

bool Logical::eval(const DoubleOperation& operation, bool first, bool second){
	return operation.perform(first, second);
}
