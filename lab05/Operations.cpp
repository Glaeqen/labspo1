#include "Operations.h"

bool OR::perform(bool first, bool second) const{
	return first || second;
}

bool AND::perform(bool first, bool second) const{
	return first && second;
}

bool XOR::perform(bool first, bool second) const{
	if(first == true && second == true) return false;
	else return first || second;
}

bool NOT::perform(bool value) const{
	return !value;
}

bool NOOP::perform(bool value) const{
	return value;
}