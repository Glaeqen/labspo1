#pragma once

#include "SingleOperation.h"
#include "DoubleOperation.h"

namespace Logical{
	// Global functions:
	// Evaluates logical value of operation for `value` 
	bool eval(const SingleOperation& operation, bool value);
	// Evaluates logical value of operation for values `first` and `second` 
	bool eval(const DoubleOperation& operation, bool first, bool second);
}

// Specific operations needed only for main function. Logical::eval requires abstraction only.
#include "Operations.h"