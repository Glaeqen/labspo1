#pragma once

// DoubleOperation abstract class to extend from for double argument logical operations
// eg. alternative

class DoubleOperation{
public:
	virtual bool perform(bool first, bool second) const = 0;
};