#include "myerrors.h"

#include <iostream>

void myerrors::handler(){
	try{
		throw;
	}
	catch(myerrors::calc_error* e){
		std::cout << "Wyjatek w: " << e->errorName_ << " [w pliku: " << e->file_ << " w lini: " << e->line_ << "]" << std::endl;;
		const myerrors::calc_error *iterErr = e;

		while(true){
			std::cout << "z powodu : ";
			if(dynamic_cast<const myerrors::calc_error*>(iterErr->previousException_)){
				const myerrors::calc_error *toDelete = iterErr;
				iterErr = dynamic_cast<const myerrors::calc_error*>(iterErr->previousException_);
				delete toDelete;
				std::cout << iterErr->errorName_ << " [w pliku: " << iterErr->file_ << " w lini: " << iterErr->line_ << "]" << std::endl;
			}
			else{
				std::cout << iterErr->previousException_->what() << std::endl;
				delete iterErr->previousException_; delete iterErr;
				return;
			}
		}

	}
}

myerrors::calc_error::calc_error(const std::runtime_error* previousException, const std::string& errorName, const std::string& file, int line)
: std::runtime_error(""), previousException_(previousException), errorName_(errorName), file_(file), line_(line)
{}

myerrors::calc_error::~calc_error() throw(){}