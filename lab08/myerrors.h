#pragma once
#include <stdexcept>
#include <string>

namespace myerrors{
	// Handler for exceptions of myerrors::calc_error* type
	void handler();
	// Exception class which can be thrown Stores information about
	// error message, in which file exception occurred and in which line.
	class calc_error : public std::runtime_error{
		//// I know that 'friending' in considered a bad practice
		//// But here it's actually useful.
		friend void handler();

		const std::runtime_error *previousException_;
		std::string errorName_;
		std::string file_;
		int line_;
	public:
		// Creates an instance of calc_error object. 
		// Parameters:
		// const std::runtime_error* previousException : pointer to the previously thrown exception,
		// const std::string& errorName : error message,
		// const std::string& file : file name in which exception occurred,
		// int line : number of line in 'file' in which exception occurred
		calc_error(const std::runtime_error* previousException, const std::string& errorName, const std::string& file, int line);
		// Destructor which has to defined.
		~calc_error() throw();
	};
}