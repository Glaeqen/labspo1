#pragma once
#include <iostream>
#include "BitManipulator.h"

using std::ostream;
using std::cout;
using std::endl;

// BitArray class is a bit array which can effectively
// manipulate its values with proper operator interface
class BitArray{
	unsigned char *array_;
	unsigned bitAmount_;
public:
	// Public constructor:
	// Creates BitArray of size `sizeInBits`. Every bit is initialised with `initValue`
	BitArray(unsigned sizeInBits, bool initValue = false);

	// Operator overload which returns value of the bit (BitManipulator is reference-to-byte wrapper).
	// `BitManipulator` object is implicitly convertable to `bool` type -->
	// --> Allows reading and writing to an array (with `bool` type).
	BitManipulator operator[](unsigned bitIndex);

	// Returns bit value as a `bool` type at specified index, read-only method.
	bool getBit(unsigned bitIndex) const;

	// Destructor:
	// Frees allocated memory.
	~BitArray();

	// Friend declaration
	friend ostream& operator<<(ostream& cout, const BitArray& bitArray);
};

// Global operator<< overload for ostream. Allows printing an array to ostream
ostream& operator<<(ostream& cout, const BitArray& bitArray);
