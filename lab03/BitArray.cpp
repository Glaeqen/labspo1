#include "BitArray.h"

BitArray::BitArray(unsigned sizeInBits, bool initValue){
	unsigned byteAmount = (sizeInBits/8)+1;
	array_ = new unsigned char[byteAmount];
	bitAmount_ = sizeInBits;

	for(int i=0; i<byteAmount; i++){
		if(initValue)	array_[i] = 255;
		else array_[i] = 0;
	}
}
BitManipulator BitArray::operator[](unsigned bitIndex){
	BitManipulator bM(array_[bitIndex/8], bitIndex%8);
	return bM;
}

bool BitArray::getBit(unsigned bitIndex) const{
	unsigned char byteMask = BitManipulator::twoPowerTo(7-(bitIndex%8));
	if(array_[bitIndex/8] & byteMask)	return true;
	return false;
}

BitArray::~BitArray(){
	delete[] array_;
	bitAmount_ = 0;
}

ostream& operator<<(ostream& cout, const BitArray& bitArray){
	for(int i=bitArray.bitAmount_-1; i>=0; i--){
		cout << bitArray.getBit(i);
		if(!(i%8)) cout << " <-" << i << " ";
	}
	return cout;
}