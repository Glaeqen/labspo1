#include "BitManipulator.h"

unsigned char BitManipulator::twoPowerTo(unsigned power){
	unsigned char result = 1;
	for(int i=0; i<power; i++){
		result *= 2;
	}
	return result;
}

BitManipulator::BitManipulator(unsigned char &byteToChange, unsigned indexInCurrentByte): byte_(byteToChange){
	indexInCurrentByte_ = indexInCurrentByte;
}

void BitManipulator::operator=(bool value){
	unsigned char byteMask = twoPowerTo(7-indexInCurrentByte_);
	if(value) byte_ |= byteMask;
	else byte_ &= ~byteMask;
}

BitManipulator::operator bool(){
	unsigned char byteMask = twoPowerTo(7-indexInCurrentByte_);
	if(byte_ & byteMask)	return true;
	return false;
}