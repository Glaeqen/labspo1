#pragma once
// BitManipulator class is a reference-to-byte wrapper
// which is capable of basic operation upon the bit in current
// byte reference.
class BitManipulator{
	unsigned char &byte_;
	unsigned indexInCurrentByte_;
public:
	// Static method which returns 2^power.
	static unsigned char twoPowerTo(unsigned power);

	// Public constructor:
	// Creates BitManipulator object initialised with proper reference
	// and index of bit in current byte destined for operations.
	BitManipulator(unsigned char &byteToChange, unsigned indexInCurrentByte);

	// Operator = overload allows changing values of the bit in current byte.
	void operator =(bool value);
	// Allow converting BitManipulator object to the `bool` type.
	operator bool();
};