#include "QKolejka.h"

QKolejka::operator bool() const{
	return !empty();
}

void QKolejka::prosze(const Person& person){
	if(person.getSex() == 'M') push_back(person);
	else                       push_front(person);
}

Person QKolejka::prosze(){
	const_iterator element = begin();
	Person result(*element);

	pop_front();
	return result;
}