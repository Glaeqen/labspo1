#pragma once
#include <iostream>

class Person{
	const char *name_;
	char sex_;
public:
	// Constructor which creates Person object with name 'name' and sex 'sex' (male by default)
	Person(const char *name, char sex = 'M');
	// Static method which returns female Person with name 'name'
	static Person kobieta(const char *name);

	const char *getName() const;
	char getSex() const;
};

std::ostream& operator<<(std::ostream& cout, const Person& person);