#include "Person.h"

Person::Person(const char *name, char sex): name_(name), sex_(sex){}

Person Person::kobieta(const char *name){
	return Person(name, 'K');
}

const char *Person::getName() const{
	return name_;
}

char Person::getSex() const{
	return sex_;
}

std::ostream& operator<<(std::ostream& cout, const Person& person){
	cout << person.getName() << " " << person.getSex() << std::flush;
	return cout;
}