#pragma once
#include "Person.h"
#include <list>

// QKolejka - ‘polite‘ container for Person objects 
class QKolejka : public std::list<Person>{
public:
	// QKolejka object can be converted to boolean indicating if it is not empty (true) 
	// or empty (false)
	operator bool() const;

	// Public method which saves person to the container
	void prosze(const Person& person);

	// Public method which removes and returns person from container
	// (female first then male)
	Person prosze();
};