#pragma once
#include <iostream>
#include <list>

class Stat{
	std::list<float> v_;
public:
	// Inserts element to the list's end
	Stat& operator()(float input);

	// Removes minimum and maximum from the list
	void dropMinMax();

	// Returns average of the elements in a container
	float average() const;
	// Returns variance of the elements in a container
	float variance() const;
	// Returns median of the elements in a container
	float median();
	// Returns minimum of the elements in a container
	float min() const;
	// Returns maximum of the elements in a container
	float max() const;

	friend std::ostream& operator<<(std::ostream& cout, const Stat& stat);
};

// Prints container to stdout
std::ostream& operator<<(std::ostream& cout, const Stat& stat);