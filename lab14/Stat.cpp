#include "Stat.h"
#include <algorithm>
#include <numeric>
#include <iterator>
#include <cmath>

Stat& Stat::operator()(float input){
	v_.push_back(input);
	return *this;
}

void Stat::dropMinMax(){
	auto minmax = std::minmax_element(v_.begin(), v_.end());
	v_.erase(minmax.first);
	v_.erase(minmax.second);
}

float Stat::average() const{
	float sum = std::accumulate(v_.begin(), v_.end(), 0.0);
	return sum/v_.size();
}

float Stat::variance() const{
	float sqSum = std::accumulate(v_.begin(), 
		v_.end(), 
		0.0, 
		[](float sum, float currentEl){
			return sum + currentEl * currentEl;
		});

	return sqrt(sqSum/v_.size() - average()*average());
}

float Stat::median(){
	v_.sort();
	auto it = v_.begin();
	std::advance(it, v_.size()/2);
	return *it;
}

float Stat::min() const{
	return *std::min_element(v_.begin(), v_.end());
}

float Stat::max() const{
	return *std::max_element(v_.begin(), v_.end());
}

std::ostream& operator<<(std::ostream& cout, const Stat& stat){
	std::ostream_iterator<float> oIter(std::cout, " ");
	cout << "[ " << std::flush; std::copy(stat.v_.begin(), stat.v_.end(), oIter); cout << "]" << std::flush;
	return cout;
}