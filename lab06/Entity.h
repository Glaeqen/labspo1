#pragma once
#include <string>

class Entity{
	std::string name_;
public:
	Entity(const std::string& name);

	std::string getName() const;
	void setName(const std::string& name);

	virtual Entity *copyElement() const = 0;

	virtual void print(int vol) const = 0;

	virtual ~Entity();

	friend std::ostream& operator<<(std::ostream& cout, const Entity& dir);
};