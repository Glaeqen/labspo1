#include "File.h"
#include <iostream>

File::File(const char *name): Entity(name){}

Entity *File::copyElement() const{
	return new File(getName().c_str());
}

void File::print(int vol) const{
	std::cout << getName() << std::endl;
}

File::~File(){}