#include "Entity.h"

Entity::Entity(const std::string& name): name_(name){}

std::string Entity::getName() const{
	return name_;
}

void Entity::setName(const std::string& name){
	name_ = name;
}

std::ostream& operator<<(std::ostream& cout, const Entity& dir){
	dir.print(2);
	return cout;
}

Entity::~Entity(){}