#pragma once
#include "Entity.h"
#include <iostream>

class Dir : public Entity{
	Entity **containedEntities_;
	unsigned volume_;
public:
	// Creates a directory named 'name'
	Dir(const char *name);

	// Returns single element from directory named 'name'
	Entity *get(const char *name);

	// Adds element to directory.
	void operator+=(Entity *entity);
	// Removes element from director named 'name'
	void operator-=(const char *name);

	// Makes deep copy of directory
	void copy(const Entity *toCopy);
	// Returns copy of itself
	virtual Entity *copyElement() const;

	// Prints directory and what it contains with 'vol' indentation
	virtual void print(int vol) const;

	virtual ~Dir();
};