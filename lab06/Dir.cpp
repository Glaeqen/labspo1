#include "Dir.h"

Dir::Dir(const char *name): Entity(name), containedEntities_(NULL), volume_(0){}

Entity *Dir::get(const char *name){
	for(int i=0; i<volume_; i++){
		if(containedEntities_[i]->getName() == name) return containedEntities_[i];
	}
}

void Dir::operator+=(Entity *entity){
	if(!containedEntities_){
		volume_ = 1;
		containedEntities_ = new Entity*[volume_];
	}
	else{
		Entity **newArray = new Entity*[volume_ + 1];
		for(int i=0; i<volume_; i++){
			newArray[i] = containedEntities_[i];
		}
		delete[] containedEntities_;
		containedEntities_ = newArray;
		volume_++;
	}

	containedEntities_[volume_-1] = entity;
}

void Dir::operator-=(const char *name){
	int index = -1;

	for(int i=0; i<volume_; i++){
		if(containedEntities_[i]->getName() == name)
			index = i;
	}

	if(index == -1) return;
	
	Entity **newArray = new Entity*[volume_-1];
	for(int i=0; i<index; i++){
		newArray[i] = containedEntities_[i];
	}
	delete containedEntities_[index];
	for(int i=index; i<volume_-1; i++){
		newArray[i] = containedEntities_[i+1];
	}
	
	delete[] containedEntities_;
	containedEntities_ = newArray;
	volume_--;
}

void Dir::copy(const Entity *toCopy){
	*this += toCopy->copyElement();
}

Entity *Dir::copyElement() const{
	Dir *newEntity = new Dir(getName().c_str());
	for(int i=0; i<volume_; i++){
		*newEntity += containedEntities_[i]->copyElement();
	}
	return newEntity;
}

void Dir::print(int vol) const{
	std::cout << getName() << std::endl;
	for(int i=0; i<volume_; i++){
		for(int j=0; j<vol; j++)  std::cout << " " << std::flush;
		containedEntities_[i]->print(vol+2);
	}
}

Dir::~Dir(){
	for(int i=0; i<volume_; i++){
		delete containedEntities_[i];
		containedEntities_[i] = NULL;
	}
	delete[] containedEntities_;
	containedEntities_ = NULL;
}
