#pragma once
#include "Entity.h"

class File : public Entity{
public:
	// Creates a file named 'name'
	File(const char *name);

	// Returns copy of itself
	virtual Entity *copyElement() const;

	// Prints name of itself
	virtual void print(int vol) const;

	virtual ~File();
};