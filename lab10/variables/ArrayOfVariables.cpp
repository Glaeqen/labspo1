#include "Lab10.h"

ArrayOfVariables::ArrayOfVariables(unsigned maxArraySize){
	maxSize_ = maxArraySize;
}

void ArrayOfVariables::Print() const{
	for(int i=0; i<array_.size(); i++){
		array_[i]->Print();
	}
}

ArrayOfVariables::~ArrayOfVariables(){
	for(int i=0; i<array_.size(); i++){
		delete array_[i];
	}
	array_.clear();
}