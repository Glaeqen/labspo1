#pragma once
#include <vector>
#include <iostream>

class VariableHolder{
public:
	virtual void Print() const = 0;
};

template <typename T>
class Variable : public VariableHolder{
	const char *name_;
	T variable_;
public:
	Variable(const char *name, T variable);
	virtual void Print() const;

	const char *getName() const{ return name_; }
	T getVariable() const{ return variable_; }
	void SetValue(T variable){ variable_ = variable; }
};

template <typename T>
Variable<T>::Variable(const char *name, T variable) : name_(name), variable_(variable) {}

template <typename T>
void Variable<T>::Print() const{
	std::cout << name_ << ": " << variable_ << std::endl;
}

typedef Variable<double> VariableDouble;
typedef Variable<float> VariableFloat;
typedef Variable<int> VariableInt;
typedef Variable<char> VariableChar;
typedef Variable<bool> VariableBool;

template <typename T>
class VariableWithUnits : public Variable<T>{
	const char *unit_;
public:
	VariableWithUnits(const char *name, T variable, const char *unit);
	virtual void Print() const;
};

template <typename T>
VariableWithUnits<T>::VariableWithUnits(const char *name, T variable, const char *unit) : Variable<T>(name, variable), unit_(unit){}

template <typename T>
void VariableWithUnits<T>::Print() const{
	std::cout << Variable<T>::getName() << ": " << Variable<T>::getVariable() << " [" << unit_ << "]" << std::endl;
}

class ArrayOfVariables{
	std::vector<VariableHolder*> array_;
	int maxSize_;
public:
	ArrayOfVariables(unsigned maxArraySize);

	template <typename T>
	Variable<T> *CreateAndAdd(const char *name, T variable);

	void Print() const;

	~ArrayOfVariables();
};

template <typename T>
Variable<T> *ArrayOfVariables::CreateAndAdd(const char *name, T variable){
	if(array_.size() + 1 >= maxSize_) throw -1;
	Variable<T> *v = new Variable<T>(name, variable);
	array_.push_back(v);
	return v;
}
