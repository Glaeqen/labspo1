#pragma once

#include <iostream>

struct AllFibonaciUnite{
	virtual void print() const = 0;
	virtual ~AllFibonaciUnite();
};

template <int input>
struct Fibonaci : public AllFibonaciUnite{
	const static int value;
	virtual void print() const;
};

template <int input>
const int Fibonaci<input>::value = Fibonaci<input - 1>::value + Fibonaci<input - 2>::value;

template <int input>
void Fibonaci<input>::print() const{
	std::cout << "Fibonaci (" << input << ") = " << Fibonaci<input>::value << std::endl;
}

template<> 
struct Fibonaci<1> : public AllFibonaciUnite{
	const static int value;
	virtual void print() const;
};

template<> 
struct Fibonaci<2> : public AllFibonaciUnite{
	const static int value;
	virtual void print() const;
};

#include <vector>

class Array{
	std::vector<AllFibonaciUnite*> vector_;
public:
	void print() const;

	void push_back(AllFibonaciUnite *f);
	AllFibonaciUnite* operator[](unsigned index);
	~Array();
};