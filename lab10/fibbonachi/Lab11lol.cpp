#include "Lab11.h"

AllFibonaciUnite::~AllFibonaciUnite(){}

const int Fibonaci<1>::value = 1;
const int Fibonaci<2>::value = 1;

void Fibonaci<1>::print() const{
	std::cout << "Fibonaci (" << 1 << ") = " << Fibonaci<1>::value << std::endl;
}

void Fibonaci<2>::print() const{
	std::cout << "Fibonaci (" << 2 << ") = " << Fibonaci<2>::value << std::endl;
}

void Array::print() const{
	for(int i=0; i<vector_.size(); i++)	vector_[i]->print();
}

void Array::push_back(AllFibonaciUnite *f){
	vector_.push_back(f);
}

AllFibonaciUnite* Array::operator[](unsigned index){
	return vector_[index];
}

Array::~Array(){
	for(int i=0; i<vector_.size(); i++){
		delete vector_[i];
	}
	vector_.clear();
}