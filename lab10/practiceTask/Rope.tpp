#include <stdexcept>

template <typename T, int nodeArraySize>
Rope<T, nodeArraySize>::Node::Node(){
	array_ = new T[nodeArraySize];
	next_ = NULL;
}

template <typename T, int nodeArraySize>
Rope<T, nodeArraySize>::Node::Node(Node *next){
	array_ = new T[nodeArraySize];
	next_ = next;
}

template <typename T, int nodeArraySize>
T& Rope<T, nodeArraySize>::Node::operator [](unsigned index){
	if(index >= nodeArraySize || index < 0) throw std::out_of_range("Index out or range");
	return array_[index];
}

template <typename T, int nodeArraySize>
typename Rope<T, nodeArraySize>::Node *Rope<T, nodeArraySize>::Node::getNext() const{
	return next_;
}

template <typename T, int nodeArraySize>
void Rope<T, nodeArraySize>::Node::setNext(Node *next){
	next_ = next;
}

template <typename T, int nodeArraySize>
Rope<T, nodeArraySize>::Node::~Node(){
	delete[] array_;
	array_ = NULL;
	if(next_) delete next_;	
}

template <typename T, int nodeArraySize>
Rope<T, nodeArraySize>::Rope(){
	head_ = tail_ = new Node;
	size_ = 0;
}

template <typename T, int nodeArraySize>
template <int nodeArraySizeInCopied>
Rope<T, nodeArraySize>::Rope(const Rope<T, nodeArraySizeInCopied>& rope){
	head_ = tail_ = new Node;
	size_ = 0;
	for(int i=0; i<rope.size(); i++){
		(*this), rope.at(i); 
	}
}

template <typename T, int nodeArraySize>
T& Rope<T, nodeArraySize>::at(unsigned index){
	if(index >= size_ || index < 0) throw std::out_of_range("Index out or range");
	int nodeIndex = index / nodeArraySize;

	Node *tmp = head_;
	for(int i=0; i<nodeIndex; i++, tmp = tmp->getNext());
	return (*tmp)[index % nodeArraySize];
}

template <typename T, int nodeArraySize>
T Rope<T, nodeArraySize>::at(unsigned index) const{
	if(index >= size_ || index < 0) throw std::out_of_range("Index out or range");
	int nodeIndex = index / nodeArraySize;

	Node *tmp = head_;
	for(int i=0; i<nodeIndex; i++, tmp = tmp->getNext());
	return (*tmp)[index % nodeArraySize];
}

template <typename T, int nodeArraySize>
int Rope<T, nodeArraySize>::capacity() const{
	return (size_ / nodeArraySize + 1) * nodeArraySize;
}
	
template <typename T, int nodeArraySize>
int Rope<T, nodeArraySize>::size() const{
	return size_;
}

template <typename T, int nodeArraySize>
Rope<T, nodeArraySize>& Rope<T, nodeArraySize>::operator ,(T input){
	if(!(size_ % nodeArraySize) && size_){
		tail_->setNext(new Node);
		tail_ = tail_->getNext();
	}
	size_++;

	at(size_ - 1) = input;
	return *this;
}

template <typename T, int nodeArraySize>
Rope<T, nodeArraySize>::~Rope(){
	if(head_) delete head_;
	head_ = tail_ = NULL;
}

template <typename U, int size>
std::ostream& operator<<(std::ostream& cout, const Rope<U, size>& rope){
	for(int i=0; i<rope.size(); i++){
		cout << rope.at(i) << ", " << std::flush;
	}
	return cout;	
}