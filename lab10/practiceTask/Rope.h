#pragma once

#include <iostream>

template <typename T, int nodeArraySize>
class Rope{
	class Node{
		T *array_;
		Node *next_;
	public:
		Node();
		Node(Node *next);

		T& operator [](unsigned index);

		Node *getNext() const;
		void setNext(Node *next);

		~Node();
	};

	Node *head_;
	Node *tail_;

	int size_; // Literal size in terms of T objects (not Nodes)
public:
	Rope();
	template<int nodeArraySizeInCopied>
	Rope(const Rope<T, nodeArraySizeInCopied>& rope);

	T at(unsigned index) const;
	int capacity() const;
	int size() const;

	T& at(unsigned index);
	Rope& operator ,(T input);

	~Rope();
};

template <typename U, int size>
std::ostream& operator<<(std::ostream& cout, const Rope<U, size>& rope);

#include "Rope.tpp"