#pragma once
#include <iostream>

template <int power>
class Unit;

template <int power>
std::ostream& operator <<(std::ostream& cout, const Unit<power>& unit);

template <int power>
class Unit{
	float value_;
public:
	Unit(float value);
	float getValue() const{return value_;}

	Unit operator+(const Unit& unit) const;
	Unit operator-(const Unit& unit) const;
	template <int rightOperandPower>
	Unit<power + rightOperandPower> operator*(const Unit<rightOperandPower>& unit) const;
	template <int rightOperandPower>
	Unit<power - rightOperandPower> operator/(const Unit<rightOperandPower>& unit) const;

	friend std::ostream& operator<<<>(std::ostream& cout, const Unit& unit);

	template <int anyPower>
	friend class Unit;
};

template <int power>
Unit<power>::Unit(float value):value_(value){}

template <int power>
Unit<power> Unit<power>::operator+(const Unit<power>& unit) const{
	return Unit<power>(value_ + unit.value_);
}

template <int power>
Unit<power> Unit<power>::operator-(const Unit<power>& unit) const{
	return Unit<power>(value_ - unit.value_);
}

template <int power>
template <int rightOperandPower>
Unit<power + rightOperandPower> Unit<power>::operator*(const Unit<rightOperandPower>& unit) const{
	return Unit<power + rightOperandPower>(value_ * unit.value_);
}

template <int power>
template <int rightOperandPower>
Unit<power - rightOperandPower> Unit<power>::operator/(const Unit<rightOperandPower>& unit) const{
	return Unit<power - rightOperandPower>(value_ / unit.value_);
}

template <int power>
std::ostream& operator <<(std::ostream& cout, const Unit<power>& unit){
	cout << unit.value_ << " m^" << power << std::flush;
	return cout;
}

// Specialisation for m^1
template <>
std::ostream& operator <<(std::ostream& cout, const Unit<1>& unit){
	cout << unit.value_ << " m" << std::flush;
	return cout;
}

typedef Unit<1> Metr;
typedef Unit<2> MetrKwadratowy;
typedef Unit<3> MetrSzescienny;